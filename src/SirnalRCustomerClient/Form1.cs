using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Client;
using Newtonsoft.Json;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace SirnalRCustomerClient
{
	public partial class Form1 : Form
	{
		HubConnection connection;

		public Form1()
		{
			InitializeComponent();
			connection = new HubConnectionBuilder()
				.WithUrl("http://localhost:5000/hubs/customer")
				.Build();

			connection.Closed += async (error) =>
			{
				await Task.Delay(new Random().Next(0, 5) * 1000);
				await connection.StartAsync();
			};
		}

		private async void buttonConnect_Click(object sender, EventArgs e)
		{
			connection.On("SendAllCustomersResponse", (Action<List<CustomerShortResponse>>)((messages) =>
			{

				foreach (var message in messages)
				{
					ShowMessage(message);
				}

			}));
			connection.On<CustomerResponse>("SendGetCustomerResponse", (message) =>
			{
				ShowMessage(message);
			});

			connection.On<CustomerResponse>("SendCreateResponse", (message) =>
			{
				ShowMessage(message);
			});

			connection.On<string>("SendEditCustomerResponse", (message) =>
			{
				this.BeginInvoke(new Action(() =>
				{
					textBox1.Text = message + Environment.NewLine;
				}));
			});

			connection.On<string>("SendDeleteCustomerResponse", (message) =>
			{
				this.BeginInvoke(new Action(() =>
				{
					textBox1.Text = message + Environment.NewLine;
				}));
			});

			try
			{
				await connection.StartAsync();
				textBox1.Text = "Connection started" + Environment.NewLine;

			}
			catch (HubException ex)
			{
				MessageBox.Show(ex.Message, "������");
			}
		}

		private void ShowMessage(object message)
		{
			this.BeginInvoke(new Action(() =>
			{

				string json = JsonConvert.SerializeObject(message, Formatting.Indented);
				textBox1.Text += json + Environment.NewLine;

			}));

		}

		private async void buttonGetAll_Click(object sender, EventArgs e)
		{
			try
			{
				await connection.InvokeAsync("GetAllCustomers");
			}
			catch (HubException ex)
			{
				MessageBox.Show(ex.Message, "������");
			}
		}

		private async void buttonGetOne_Click(object sender, EventArgs e)
		{
			try
			{
				await connection.InvokeAsync("GetCustomer", Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"));
			}
			catch (HubException ex)
			{
				MessageBox.Show(ex.Message, "������");
			}
		}

		private async void buttonCreate_Click(object sender, EventArgs e)
		{
			try
			{
				CreateOrEditCustomerRequest request = new CreateOrEditCustomerRequest
				{
					Email = "1111@qqq",
					FirstName = "bob",
					LastName = "Bob",
					PreferenceIds = []
				};
				await connection.InvokeAsync("CreateCustomer", request);
			}
			catch (HubException ex)
			{
				MessageBox.Show(ex.Message, "������");
			}
		}

		private async void buttonDelete_Click(object sender, EventArgs e)
		{
			try
			{
				await connection.InvokeAsync("DeleteCustomer", Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"));
			}
			catch (HubException ex)
			{
				MessageBox.Show(ex.Message, "������");
			}
		}

		private async void buttonEdit_Click(object sender, EventArgs e)
		{
			try
			{
				CreateOrEditCustomerRequest request = new CreateOrEditCustomerRequest
				{
					Email = "1111@qqq",
					FirstName = "bob",
					LastName = "Bob",
					PreferenceIds = []
				};
				await connection.InvokeAsync("EditCustomers", Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), request);
			}
			catch (HubException ex)
			{
				MessageBox.Show(ex.Message, "������");
			}
		}
	}
}
